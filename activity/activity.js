// 3.
db.users.insert(
{
	name: "single",
	accomodates: 2,
	price: 1000,
	discription: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
}
)

// 4.
db.users.insertMany([
{
	name: "double",
	accomodates: 3,
	price: 2000,
	discription: "A room fit for a small family going on a vacation",
	rooms_available: 5,
	isAvailable: false
},
{
	name: "queen",
	accomodates: 4,
	price: 4000,
	discription: "A room with a queen sized bed perfect for a simple getaway",
	rooms_available: 15,
	isAvailable: false
}
])

// 5.
db.users.find({name: "double"});

// 6.
db.users.updateOne(
{
	name: "queen"
},
{
	$set: {
		rooms_available: 0
	}
}
)

// 7.
db.users.deleteMany({
	rooms_available: 0
})
