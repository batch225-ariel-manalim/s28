// MongoDB Operations
// For creating or inserting data into database
db.users.insert({
	firstName: "Ely",
	lastName: "Buendia",
	age: 50,
	contact: {
		phone: "534776",
		email: "ely@eheads.com"
	},
	courses: ["CSS", "Javascript", "Python"],
	department: "none"
})





// FOR INSERTING MANY DOCUMENTS
db.users.insertMany([
{
	name: "single",
	accomodates: 2,
	price: 1000,
	discription: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
},
{
	name: "double",
	accomodates: 3,
	price: 2000,
	discription: "A room fit for a small family going on a vacation",
	rooms_available: 5,
	isAvailable: false
}
])






// FIND ALL
// for querying all the data in database
db.users.find();

// FIND ONE
db.users.find({firstName: "Ely", age: 50});





// DELETE ONE
db.users.deleteOne({
	firstName: "Chito"
})

// DELETE ALL
db.users.deleteMany({

})






//UPDATE ONE
db.users.updateOne(
{
	firstName: "Ely"
},
{
	$set: {
		firstName: "Gloc9",
		lastName: "Wala siyang apelyedo"
	}
}
)

// UPDATE MANY
db.users.updateMany(
{
	firstName: "Ely"
},
{
	$set: {
		firstName: "Gloc9",
		lastName: "Wala siyang apelyedo"
	}
}
)